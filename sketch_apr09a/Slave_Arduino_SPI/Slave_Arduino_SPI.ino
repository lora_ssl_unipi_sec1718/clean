#include<SPI.h>
char buff[100];
volatile byte index;
volatile bool receivedone;

void setup() {
Serial.begin(9600);
SPCR |= bit(SPE);
pinMode(MISO, OUTPUT);//Master in slave out
index =0;
receivedone = false;
SPI.attachInterrupt();

}

void loop(void) {
  if (receivedone){
    buff[index]=0;
    Serial.println(buff);
    index=0;
    receivedone = false;   
    }
}


ISR (SPI_STC_vect){
  uint8_t oldsrg = SREG;
  cli();
  char c =SPDR;
  if (index< sizeof buff){
    buff[index++]=c;
    if(c == '\n'){
      receivedone = true;      
      }   
    }  
  SREG=oldsrg;
  }



